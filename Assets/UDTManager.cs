﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class UDTManager : MonoBehaviour, IUserDefinedTargetEventHandler
{

	UserDefinedTargetBuildingBehaviour udt_targetBuildingBehaviour;

	ObjectTracker objectTracker;
	DataSet dataSet;

	ImageTargetBuilder.FrameQuality udt_FrameQuality;
	public ImageTargetBehaviour targetBehaviour;

	int targetCounter;

	//when we move the camera this runs to give us results for the quality of the video feed
	public void OnFrameQualityChanged(ImageTargetBuilder.FrameQuality frameQuality)
	{
		//throw new System.NotImplementedException();
		udt_FrameQuality = frameQuality;
	}

	public void OnInitialized()
	{
		//throw new System.NotImplementedException();
		objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
		if (objectTracker!=null) //we got the tracker
		{
			dataSet = objectTracker.CreateDataSet();
			objectTracker.ActivateDataSet(dataSet);
		}
	}

	public void OnNewTrackableSource(TrackableSource trackableSource)
	{
		//throw new System.NotImplementedException();
		targetCounter++;
		objectTracker.DeactivateDataSet(dataSet);

		dataSet.CreateTrackable(trackableSource, targetBehaviour.gameObject);

		
		objectTracker.ActivateDataSet(dataSet);
		udt_targetBuildingBehaviour.StartScanning();

	}

	public void BuildTarget()
	{
		if(udt_FrameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_HIGH)
		{
			//BUILDING A NEW TARGET
			udt_targetBuildingBehaviour.BuildNewTarget(targetCounter.ToString(), targetBehaviour.GetSize().x);
		}
		else if (udt_FrameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_MEDIUM)
		{

		}
		else if (udt_FrameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_LOW)
		{

		}
	}

	// Start is called before the first frame update
	void Start()
    {
		udt_targetBuildingBehaviour = GetComponent<UserDefinedTargetBuildingBehaviour>(); //get UserDefinedTargetBuildingBehaviour
		if (udt_targetBuildingBehaviour)// user defined target bahaviour has been found
		{
			udt_targetBuildingBehaviour.RegisterEventHandler(this);
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
