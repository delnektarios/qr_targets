﻿using UnityEngine;
using System;
using System.Collections;
using Vuforia;
using System.Threading;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.Video;

public class CameraScript : MonoBehaviour
{
    public UnityEngine.UI.RawImage rawImage;
    //public Canvas canvas;
    //public Material init_material;
    //public Material project_material;
    //public UnityEngine.UI.Text textinfo;
    //public UnityEngine.UI.Button fullscreen_button;
    //public UnityEngine.UI.Button exit_fullscreen_button;
    //public UnityEngine.UI.Button closeButton;
    //public UnityEngine.UI.RawImage scan_area;

    //public Sprite fullscreen;
    //public Sprite exitFullscreen;

    //public float step;
    //public float limit_zoom;

    //float add_step;

    //private data members
    private bool cameraInitialized;
    private IBarcodeReader barCodeReader;
    private Texture3D texture3D;
    private CameraDevice cameraInstance;
    private Result currentQRScannedData;
    private long counter;
    private PIXEL_FORMAT mPixelFormat;
    QRCodeReader reader;
    CameraDevice.VideoModeData videoModeData;
    private ResultPoint[] point;
    private Vector3 world_point = new Vector3();
    //private Camera cam;

    //public VideoClip videoToPlay;
    private VideoPlayer videoPlayer;
    private VideoSource videoSource;
    private AudioSource audioSource;
    //https://answers.unity.com/questions/300864/how-to-stop-a-co-routine-in-c-instantly.html
    private Coroutine my_coroutine;
    //public VideoPlayer rawVideo;
    private bool downloading = false;
    private float shrink_top = 0.25f;
    private float shrink_left = 0.25f;
    private float shrink_height = 0.5f;
    private float shrink_width = 0.5f;

    void Start()
    {
        //rawImage.material = init_material;
        //closeButton.gameObject.SetActive(false);
        //fullscreen_button.gameObject.SetActive(false);
        //exit_fullscreen_button.gameObject.SetActive(false);
        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
        vuforia.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
    }

    private void OnVuforiaStarted()
    {
        Debug.Log("OnVuforiaStarted");
        cameraInstance = CameraDevice.Instance;
        cameraInstance.SetFrameFormat(PIXEL_FORMAT.GRAYSCALE, true);
        mPixelFormat = PIXEL_FORMAT.GRAYSCALE;
        Debug.Log("PIXEL FORMAT is " + mPixelFormat);
        cameraInstance.GetVideoMode(CameraDevice.CameraDeviceMode.MODE_DEFAULT);
        cameraInstance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        ConfigureBarcodeScanner();
        cameraInitialized = true;
        Debug.Log("Screen width" + Screen.width * shrink_width);
        Debug.Log("Screen height" + Screen.height * shrink_height);
    }


    private void ConfigureBarcodeScanner()
    {
        reader = new QRCodeReader();
        barCodeReader = new BarcodeReader(reader, null, (src)=> {return new HybridBinarizer(src); });
        barCodeReader.ResultFound += (obj) => {
            Debug.Log("OnResultScanned");
            if (obj != null)
			{
				StartCoroutine(LoadFromWeb(obj.Text));
			}
            
            //VERY IMPORTANT NOTES
            //https://docs.unity3d.com/ScriptReference/Camera.ScreenToWorldPoint.html
            //https://stackoverflow.com/questions/50073719/using-zxing-in-unity-to-locate-qrcodes-position-pattern
            point = obj.ResultPoints;
            Debug.Log("qr code position : " + point[0].X + " and " + point[1].Y);
            //world_point = Camera.main.ScreenToWorldPoint(new Vector3(point[0].X, point[1].Y, Camera.main.nearClipPlane));
        };
        barCodeReader.Options = new DecodingOptions();
        //barCodeReader.Options.TryHarder = true;
    }

 
    private void BarCodeReader_ResultFound(Result obj)
    {
       

    }
                  
    void OnTrackablesUpdated()
    {
        if (cameraInitialized && !downloading)
        {
            Image cameraFrame = null;
            try
            {   if(counter % 10 == 0) 
                   cameraFrame = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.GRAYSCALE);
                counter++;
            }
            catch (Exception e) { Debug.LogError(e.Message); }
            if (cameraFrame == null)
            {
                Debug.Log("CameraFeed is null");
                return;
            }
            else if ( cameraFrame.BufferWidth > 0 && cameraFrame.BufferHeight > 0)
            {
                Debug.Log(" Decode Frame ");
                RGBLuminanceSource src = new RGBLuminanceSource(cameraFrame.Pixels, cameraFrame.BufferWidth, cameraFrame.BufferHeight, RGBLuminanceSource.BitmapFormat.Gray8);
                RGBLuminanceSource croppedSrc = (RGBLuminanceSource)src.crop(Convert.ToInt32(cameraFrame.BufferWidth * 0.25),
                Convert.ToInt32(cameraFrame.BufferHeight * 0.25), Convert.ToInt32(cameraFrame.BufferWidth * 0.5), Convert.ToInt32(cameraFrame.BufferHeight * 0.5));

                barCodeReader.Decode(src);
                barCodeReader.Decode(croppedSrc);
                //or
                // barCodeReader.Decode(cameraFrame.Pixels, cameraFrame.BufferWidth, cameraFrame.BufferHeight, RGBLuminanceSource.BitmapFormat.BGR32);
            }
        }
        if (Int64.MaxValue - counter < 1000)
        {
            Debug.Log("Resetting counter");
            counter = 0;
        }
    }



   
    /// <summary>
    /// What is happening here
    /// Why use 2 requests?
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>

    IEnumerator LoadFromWeb(string url)
    {

        Debug.Log("Fetching data from URL decoded...");
        UnityWebRequest webRequest = UnityWebRequest.Get(url);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError)
            Debug.Log("A network error happened");
        else {
            Dictionary<string, string> content_info = webRequest.GetResponseHeaders();
            foreach (KeyValuePair<string, string> pair in content_info)
                Debug.Log("key : " + pair.Key + " and Value : " + pair.Value);
        }

        UnityWebRequest wr = new UnityWebRequest(url);
        DownloadHandlerTexture texDl = new DownloadHandlerTexture(true);
        wr.downloadHandler = texDl;
        cameraInitialized = false;
        yield return wr.SendWebRequest();

        //checking if content is video or image

        if (!(wr.isNetworkError || wr.isHttpError))
        {
            Debug.Log("DownloadedImage");

            //here we check the content type and act accordingly
            Dictionary<String, String> content_info = webRequest.GetResponseHeaders();

            //Types of Digital Image Files: TIFF, JPEG, GIF, PNG
            bool isImage = content_info.ContainsValue("image/*");

            if (content_info.ContainsValue("image/tiff") || content_info.ContainsValue("image/jpeg")
                || content_info.ContainsValue("image/gif") || content_info.ContainsValue("image/png"))
            {
                Debug.Log("we have an image!");
                //the user see the object. no scanning needed
               // cameraInitialized = false;
                texture2D = texDl.texture;
                //rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
                //canvas.GetComponent<RectTransform>().position = new Vector3(point[0].X, point[1].Y);
                //rawImage.GetComponent<RectTransform>().position = new Vector2(point[0].X, point[1].Y);
                rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture3D.width, texture3D.height);
                RectTransform rtr= rawImage.GetComponent<RectTransform>();
                Vector3[] vc4 = new Vector3[4];
                rtr.GetLocalCorners(vc4);
                for (var i = 0; i < 4; i++)
                {
                    Debug.Log("Local Corner " + i + " : " + vc4[i]);
                }
                //rawImage.material = project_material;
                //rawImage.texture = texture2D;
                //closeButton.gameObject.SetActive(true);
                //fullscreen_button.gameObject.SetActive(true);
                //exit_fullscreen_button.gameObject.SetActive(true);
            }
            else
            {
                Debug.Log("video is here");
                //Play_Video(url);
            }

            //-------------------------------------------------
            // QRCode detected.
            Debug.Log(url);
            //Application.OpenURL(data.Text);      // our function to call and pass url as text
            //textinfo.text = url;
            cameraInitialized = true;
            ///////////////
        }
    }

    public void close()
    {
        //rawImage.material = init_material;
        //closeButton.gameObject.SetActive(false);
        //fullscreen_button.gameObject.SetActive(false);
        //exit_fullscreen_button.gameObject.SetActive(false);
        //rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(640, 480);
        //rawImage.uvRect = new Rect(0, 0, 100, 100);
        //rawImage.material = init_material;
        //scanRegion.gameObject.SetActive(true);

        //when an object is present dont scan until close burron is pressed
        cameraInitialized = true;
    }

  
   

    public void enter_fullscreen()
    {
		/*
        //to avoid over zooming the object
        if (Math.Abs(add_step) < Math.Abs(limit_zoom))
        {
            add_step -= step;
        }*/

        //https://forum.unity.com/threads/setting-pos-z-in-recttransform-via-scripting.270230/
        //newbutton.GetComponent<RectTransform>().localPosition = new Vector3(1,2,3);
        //rawImage.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, add_step);
        //exit_fullscreen_button.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, -zoom);

        //fullscreen_button.image.sprite = fullscreen;

        //fullscreen_button.gameObject.SetActive(false);
        //exit_fullscreen_button.gameObject.SetActive(true);

    }

    public void exit_fullscreen()
    {
		/*
        if (Math.Abs(add_step) > 1)
        {
            add_step += step;
        }*/

        //rawImage.uvRect.height = 0.0f;
        //rawImage.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, add_step);
        //exit_fullscreen_button.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, -1);

        //fullscreen_button.gameObject.SetActive(true);
        //exit_fullscreen_button.gameObject.SetActive(false);
    }

    //https://answers.unity.com/questions/1172061/how-to-change-image-of-button-when-clicked.html



	/*
    //video player 
    public void Play_Video(String video_url)
    {
        Application.runInBackground = true;
        my_coroutine = StartCoroutine(playVideo(video_url));
    }

    public void Stop_Video()
    {
        videoPlayer.Stop();
        audioSource.Stop();
        Application.runInBackground = false;
        StopCoroutine(my_coroutine);
    }

    //handling videos here
    IEnumerator playVideo(String video_url)
    {

        //Add VideoPlayer to the GameObject
        videoPlayer = gameObject.AddComponent<VideoPlayer>();

        //Add AudioSource
        audioSource = gameObject.AddComponent<AudioSource>();

        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();

        //We want to play from video clip not from url

        videoPlayer.source = VideoSource.VideoClip;

        // Video clip from Url
        //https://answers.unity.com/questions/1370621/using-videoplayer-to-stream-a-video-from-a-website.html
        //videoPlayer.source = VideoSource.Url;
        //videoPlayer.url = "https://dl.dropbox.com/s/d4f4v2df1lhtz5q/DEMO%201%2027-36%281%29%281%29.mp4";

        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = video_url;


        //Set Audio Output to AudioSource
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.controlledAudioTrackCount = 1;
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        //videoPlayer.clip = videoToPlay;
        videoPlayer.Prepare();

        //WaitForSeconds waitTime = new WaitForSeconds(5);
        //Wait until video is prepared
        while (!videoPlayer.isPrepared)
        {
            yield return null;
            //yield return waitTime;
        }

        Debug.Log("Done Preparing Video");

        //rawVideo.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.height*shrink_height, Screen.width*shrink_width);
        //Assign the Texture from Video to RawImage to be displayed
        rawVideo = videoPlayer;

        //Play Video
        videoPlayer.Play();

        //Play Sound
        audioSource.Play();

        Debug.Log("Playing Video");
        while (videoPlayer.isPlaying)
        {
            Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
            yield return null;
        }

        Debug.Log("Done Playing Video");
    }
	*/


    void OnPaused(bool paused)
    {
        Debug.Log("OnPaused");
        if (paused) cameraInitialized = false;
        if (!paused) // resumed
        {
            // Set again autofocus mode when app is resumed
            cameraInitialized = true;
            cameraInstance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }



    /// <summary>
    /// Unused
    /// </summary>
    /// <param name="camerafeed"></param>
    private void convertImage(Image camerafeed)
    {
        texture3D = new Texture3D(camerafeed.BufferWidth, camerafeed.BufferHeight, 0,  TextureFormat.RGBA32, false);
        //texture2D.LoadImage(camerafeed.Pixels);
        texture3D.LoadRawTextureData(camerafeed.Pixels);
        texture3D.Apply();
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(Convert.ToInt32(0.75* texture3D.width), texture3D.height);
        //rawImage.material = project_material;
        // rawImage.material.SetTexture("img",texture2D);
        rawImage.texture = texture3D;
    }


    /** Unused **/
    private RGBLuminanceSource trackWithRGBLuminance(Image camerafeed)
    {
        string imageInfo = " Frame Properties: ";
        imageInfo += " 1. size: width = " + camerafeed.Width + " x height = " + camerafeed.Height + "\n";
        imageInfo += " 2. bufferSize: " + camerafeed.BufferWidth + " x " + camerafeed.BufferHeight + "\n";
        imageInfo += " 3. stride: " + camerafeed.Stride;
        Debug.Log(imageInfo);
        RGBLuminanceSource src = new RGBLuminanceSource(camerafeed.Pixels, camerafeed.BufferWidth, camerafeed.BufferHeight, RGBLuminanceSource.BitmapFormat.Gray8);
        Debug.Log("Attempting loading of source with RGBLuminance has width = " + src.Width + "height = " + src.Height);
        RGBLuminanceSource croppedSource;
        byte[] croppedImage;
        if (src.CropSupported)
        {
            int left = Convert.ToInt32(camerafeed.BufferWidth * shrink_left);
            int top = Convert.ToInt32(camerafeed.BufferHeight * shrink_top);
            int width = Convert.ToInt32(camerafeed.BufferWidth * shrink_width);
            int height = Convert.ToInt32(camerafeed.BufferHeight * shrink_height);
            Debug.Log("Cropping image with values left = " + left + " top = " + top + " width = " + width + " height = " + height);
            croppedSource = (RGBLuminanceSource)src.crop(left, top, width, height);
            Debug.Log("Height is " + croppedSource.Height + " Width is " + croppedSource.Width);
            croppedImage = croppedSource.Matrix;

            //LuminanceSource inverted = croppedSource.invert();
            //PreferBinarySerialization


            texture2D = new Texture2D(croppedSource.Width, croppedSource.Height, TextureFormat.Alpha8, false);
            //texture2D.LoadImage(camerafeed.Pixels);
            texture2D.LoadRawTextureData(croppedSource.Matrix);
            texture2D.Apply();
            rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
            //rawImage.material = project_material;
            rawImage.material.SetTexture("img", texture2D);
            rawImage.texture = texture2D;
            return croppedSource;
        }


        return null;
    }


    //unused
    private RGBLuminanceSource CreateRGBLuminanceSource(Color32[] bytes, int width, int height)
    {
        Debug.Log("CreateLuminanceSource");
        Color32LuminanceSource src = new Color32LuminanceSource(bytes, 640, 480);

        RGBLuminanceSource rGBLuminanceSource = (RGBLuminanceSource)src.crop(Convert.ToInt32(640 * 0.25), Convert.ToInt32(480 * 0.25),
                                                             Convert.ToInt32(640 * 0.5), Convert.ToInt32(480 * 0.5));
        return rGBLuminanceSource;

    }

    //unused
    IEnumerator QRdecode(Image camerafeed)
    {

        Debug.Log("Height is  " + camerafeed.BufferHeight + " Width is " + camerafeed.BufferWidth);
        string imageInfo = " image: \n";
        imageInfo += " size: " + camerafeed.Width + " x " + camerafeed.Height + "\n";
        imageInfo += " bufferSize: " + camerafeed.BufferWidth + " x " + camerafeed.BufferHeight + "\n";
        imageInfo += " stride: " + camerafeed.Stride;
        Debug.Log(imageInfo);
        HybridBinarizer binarizer = new HybridBinarizer(trackWithRGBLuminance(camerafeed));

        yield return reader.decode(new BinaryBitmap(binarizer));
        //yield return barCodeReader.Decode(trackWithRGBLuminance(camerafeed));
    }
    //never hit
    private void ResultPointFound(ResultPoint point)
    {
        Debug.Log("what");
        barCodeReader.Options.TryHarder = true;
    }

    //never used
    private LuminanceSource CreateLuminanceSource(Color32[] bytes, int width, int height)
    {
        Debug.Log("CreateLuminanceSource");
        Color32LuminanceSource src = new Color32LuminanceSource(bytes, 640, 480);

        RGBLuminanceSource rGBLuminanceSource = (RGBLuminanceSource)src.crop(Convert.ToInt32(640 * 0.25), Convert.ToInt32(480 * 0.25),
                                                             Convert.ToInt32(640 * 0.5), Convert.ToInt32(480 * 0.5));
        return rGBLuminanceSource;

    }

}
